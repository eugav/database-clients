package ru.mai.dep806.catalog.model;

import javax.persistence.*;

@Entity @DiscriminatorValue("MOVIE")
public class MovieItem extends CatalogItem {
	
	public enum MovieGenre {COMEDY, TRILLER, ACTION, MUSIC};

	@Column(name="GENRE")
	@Enumerated(EnumType.STRING)
	private MovieGenre genre;

	public MovieGenre getGenre() {
		return genre;
	}
	public void setGenre(MovieGenre genre) {
		this.genre = genre;
	}

}
