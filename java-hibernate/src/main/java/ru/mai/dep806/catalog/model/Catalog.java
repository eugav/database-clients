package ru.mai.dep806.catalog.model;

import javax.persistence.*;
import java.util.List;

@javax.persistence.SequenceGenerator(
        name = "SEQ_ID",
        sequenceName = "SEQ_ID"
)
@Entity
@Table(name = "CATALOG")
public class Catalog {

    @Id
    @GeneratedValue(generator = "SEQ_ID")
    @Column(name = "ID")
    private Long id;

    @Version
    private Long version;

    @Column(name = "NAME")
    private String name;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "catalog")
    private List<CatalogItem> items;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CatalogItem> getItems() {
        return items;
    }

    public void setItems(List<CatalogItem> items) {
        this.items = items;
    }
}
