package ru.mai.dep806.catalog.dao.hibernate;

import org.hibernate.Session;
import org.hibernate.query.Query;
import ru.mai.dep806.catalog.model.Catalog;
import ru.mai.dep806.catalog.model.CatalogItem;
import ru.mai.dep806.catalog.util.HibernateUtil;

import java.util.List;
import java.util.Map;

public class CatalogDAO {

    /**
     * Получение каталога по его идентификатору
     * @param id идентификатор каталога
     * @return каталог
     */
	public Catalog getCatalog(Long id) {
        return HibernateUtil.getSession().get(Catalog.class, id);
	}
	
    /**
     * Получение списка всех каталогов
     * @return список каталогов
     */
	public List<Catalog> getCatalogs() {
        return HibernateUtil.getSession().createQuery("from Catalog", Catalog.class).list();
	}
	
    /**
     * Сохранение (добавление или обновление)
     * каталога и всех связанных с ним сущностей
     * @param catalog каталог для сохранения
     * @return сохраненный каталог
     */
	public Catalog saveCatalog(Catalog catalog) {
		Session session = HibernateUtil.getSession();
        session.beginTransaction();
        session.saveOrUpdate(catalog);
        session.getTransaction().commit();
        return catalog;
	}
	
    /**
     * Удаление каталога
     */
	public void deleteCatalog(Catalog catalog) {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        session.delete(catalog);
        session.getTransaction().commit();
    }
	
    //Примеры запросов
    /**
     * Объединение связанных таблиц через "точечный" синтаксис, привязка параметров
     * Поиск по всем каталогам по подстроке имени автора
     */
	public List<CatalogItem> findItemsByAuthorName(String name) {
        Session session = HibernateUtil.getSession();
        Query<CatalogItem> query =
                session.createQuery("from CatalogItem as item where item.author.name like :name", CatalogItem.class);
		query.setParameter("name", "%"+name+"%");

        return query.list();
	}
	
    /**
     *  Группировка, конструкция select new map
     *  Возвращает все каталоги и количество элементов в нем
     *  в виде списка ассоциативных массивов
     */
	public List<Map<String,Object>> getStatistics() {
        Session session = HibernateUtil.getSession();
        Query query = session.createQuery(
                "select new map(catalog.name as Название, (select count(*) from catalog.items) as Количество) from Catalog as catalog");
        return query.list();
	}
	
}
