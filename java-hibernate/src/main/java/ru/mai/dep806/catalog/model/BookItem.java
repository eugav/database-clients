package ru.mai.dep806.catalog.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity @DiscriminatorValue("BOOK")
public class BookItem extends CatalogItem {
	@Column(name="ISBN")
	private String ISBN;

	public String getISBN() {
		return ISBN;
	}

	public void setISBN(String isbn) {
		ISBN = isbn;
	}
	
}
