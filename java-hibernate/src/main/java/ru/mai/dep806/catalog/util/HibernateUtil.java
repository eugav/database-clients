package ru.mai.dep806.catalog.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {

	private static SessionFactory sessionFactory;

    private static Session session = null;

    private static SessionFactory getSessionFactory() {
        if (sessionFactory==null || sessionFactory.isClosed()) {
			sessionFactory = new Configuration().configure().buildSessionFactory();
		}
        return sessionFactory;
    }

    public static Session getSession() {

        if (session==null || !session.isOpen()) {
            session = getSessionFactory().openSession();
        }
        return session;
	}

    public static void close() {
        if (session != null && session.isOpen()) {
            session.close();
        }
        if (sessionFactory != null && !sessionFactory.isClosed()) {
            sessionFactory.close();
        }
    }
}
