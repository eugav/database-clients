package ru.mai.dep806.catalog;

import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import ru.mai.dep806.catalog.model.*;
import ru.mai.dep806.catalog.util.HibernateUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Пример использования Hibernate ORM API.
 */
public class CatalogDemo {

    public static void main(String[] args) {

        // Создание нового каталога и заполнение данными
        Catalog catalog = new Catalog();
        catalog.setName("Мой каталог №1");
//        catalog.setId(1L);

        List<CatalogItem> items = new ArrayList<>();

        Author fauler = new Author();
        fauler.setName("Мартин Фаулер");
        fauler.setBirthdate(new Date());

        Author burton = new Author();
        burton.setName("Тим Бартон");
        burton.setBirthdate(new Date());

        BookItem book = new BookItem();
        book.setAuthor(fauler);
        book.setName("UML Основы");
        book.setISBN("1-232-12345-2");
        book.setCatalog(catalog);

        MovieItem movie = new MovieItem();
        movie.setAuthor(burton);
        movie.setName("Кошмар перед рождеством");
        movie.setGenre(MovieItem.MovieGenre.MUSIC);
        movie.setCatalog(catalog);

        items.add(book);
        items.add(movie);

        catalog.setItems(items);

        // Получим сессию Hibernate
        Session session = HibernateUtil.getSession();

        // Начало транзакции
        session.beginTransaction();

        // Сохранение каталога в базу данных
        session.saveOrUpdate(catalog);
        // Завершение транзакции
        session.getTransaction().commit();


        System.out.println("Id=" + catalog.getId());


        Catalog cat1 = session.get(Catalog.class, 41L);

        List<Catalog> catalogs = session.createQuery("from Catalog", Catalog.class).list();

            for (Catalog c : catalogs) {
                    c.getItems().get(0);
            }

        // Вывод списка каталогов
        List<Catalog> list = session
                .createQuery("select c from Catalog c where c.name like '%1%'", Catalog.class)
                .list();

        List<CatalogItem> list1 = session
                .createQuery("from CatalogItem i where i.author.name like '%Фаулер%'", CatalogItem.class)
                .list();
//
        DetachedCriteria criteria = DetachedCriteria.forClass(Catalog.class);
            String name = "";
        if (name != null && name.trim().length() > 0) {
            criteria.add(Restrictions.like("name", "%" + name + "%"));
                criteria.createCriteria("author", "a");

        }
//
        criteria.getExecutableCriteria(session).setFirstResult(100).setMaxResults(20);

        session.createQuery("from Catalog c join fetch c.items").list();
        session.createQuery("from BookItem").list();
        session.createQuery("from MovieItem").list();
        session.createQuery("from CatalogItem").list();

        HibernateUtil.close();
    }
}
