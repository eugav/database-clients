package ru.mai.dep806.catalog.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Id;

@javax.persistence.SequenceGenerator(
    name="SEQ_ID",
    sequenceName="SEQ_ID"
)
@Entity @Table(name="AUTHOR")
public class Author {

    @Id @GeneratedValue(generator="SEQ_ID")
    private Long id;

    @Column(name="NAME")
    private String name;

    @Column(name="BIRTHDATE")
    private Date birthdate;

    @OneToMany(cascade=CascadeType.REFRESH, fetch=FetchType.LAZY)
    @JoinColumn(name="AUTHOR_ID")
    private List<CatalogItem> items;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public List<CatalogItem> getItems() {
		return items;
	}
	public void setItems(List<CatalogItem> items) {
		this.items = items;
	}
}
