package ru.mai.dep806.catalog.dao.hibernate;

import org.junit.After;
import org.junit.Test;
import ru.mai.dep806.catalog.model.*;
import ru.mai.dep806.catalog.util.HibernateUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

/**
 * Тест для CatalogDAO.
 */
public class CatalogDAOTest {

    private CatalogDAO dao = new CatalogDAO();

    @After
    public void close() {
        HibernateUtil.close();
    }

    @Test
    public void saveCatalog() throws Exception {
        // Подготовка данных
        Catalog catalog = createNewCatalog();

        // Действие
        catalog = dao.saveCatalog(catalog);

        // Проверка
        assertNotNull(catalog.getId());

        HibernateUtil.getSession().close();
        Catalog newCatalog = HibernateUtil.getSession().get(Catalog.class, catalog.getId());
        assertNotNull(newCatalog);
        assertEquals("Мой каталог 1", newCatalog.getName());
        assertEquals(2, newCatalog.getItems().size());
    }

    @Test
    public void deleteCatalog() throws Exception {
        // Подготовка данных
        Catalog catalog = new Catalog();
        catalog.setName("Мой каталог 1");

        catalog = dao.saveCatalog(catalog);
        assertNotNull(catalog.getId());

        HibernateUtil.getSession().close();

        // Действие
        dao.deleteCatalog(catalog);

        // Проверка
        Catalog newCatalog = dao.getCatalog(catalog.getId());
        assertNull(newCatalog);
    }

    @Test
    public void findItemsByAuthorName() throws Exception {

        // Подготовка данных
        Catalog catalog = createNewCatalog();
        dao.saveCatalog(catalog);
        HibernateUtil.getSession().close();

        // Действие
        List<CatalogItem> items = dao.findItemsByAuthorName("%Мартин%");
        List<CatalogItem> myItems = items.stream()
                .filter(item -> item.getCatalog().getId().equals(catalog.getId()))
                .collect(Collectors.toList());

        // Проверка
        assertEquals(1, myItems.size());
        assertTrue(myItems.get(0).getAuthor().getName().contains("Мартин"));
    }

    private Catalog createNewCatalog() {
        Catalog catalog = new Catalog();
        catalog.setName("Мой каталог 1");

        List<CatalogItem> items = new ArrayList<>();

        Author fauler = new Author();
        fauler.setName("Мартин Фаулер");
        fauler.setBirthdate(new Date());

        Author burton = new Author();
        burton.setName("Тим Бартон");
        burton.setBirthdate(new Date());

        BookItem book = new BookItem();
        book.setAuthor(fauler);
        book.setName("UML Основы");
        book.setISBN("1-232-12345-2");
        book.setCatalog(catalog);

        MovieItem movie = new MovieItem();
        movie.setAuthor(burton);
        movie.setName("Кошмар перед рождеством");
        movie.setGenre(MovieItem.MovieGenre.MUSIC);
        movie.setCatalog(catalog);

        items.add(book);
        items.add(movie);

        catalog.setItems(items);
        return catalog;
    }

}